import java.util.Arrays;

public class BubbleSort {

    public static void bubbleSort(int[] array){
        //i代表趟数
        for (int i = 0; i < array.length-1; i++) {
            boolean flg=false;
            for (int j = 0; j < array.length-1-i; j++) {//j的取值范围多多注意
                if(array[j]>array[j+1]){
                    int tmp=array[j];
                    array[j]=array[j+1];//i不参与，只代表趟数
                    array[j+1]=tmp;
                    flg = true;
                }
            }
            if(flg == false){//没交换表示已排好序，不用再遍历直接输出
                return;
            }
        }
    }

    public static void main(String[] args) {
        int[] array={11,21,12,14,7,4};
        System.out.println("排序前数组："+ Arrays.toString(array));
        bubbleSort(array);//默认从小到大排序
        System.out.println("排序后的数组："+Arrays.toString(array));
    }
}
