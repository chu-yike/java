import java.util.Random;
import java.util.Scanner;

public class jNumberGuess {
    public static void main(String[] args) {
        Random random = new Random();//默认随机种子是系统时间
        Scanner sc = new Scanner(System.in);
        int toGuess = random.nextInt(100);//生成【0，100）的随机数
        System.out.println("toGuess:" + toGuess);
        while (true) {
            System.out.println("请输入一个数字:(0~100)");
            int num = sc.nextInt();
            if (num < toGuess) {
                System.out.println("猜小了");
            } else if (num > toGuess) {
                System.out.println("猜大了");
            } else {
                System.out.println("猜对惹>.<");
                break;
            }
        }
    }
}