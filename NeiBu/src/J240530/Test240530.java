package J240530;

class Out{
    public void test(){
        System.out.println("匿名类重写");
    }
}



interface IA{
    void test();    //接口
}



public class Test240530 {
    public static void main(String[] args) {
        //实例化一个静态内部类对象
        OutClass.InnerClass innerClass1 = new OutClass.InnerClass();
        innerClass1.test();

        OutClass.INInnerClass innerClass2 = new OutClass.INInnerClass();
        innerClass2.test();
        OutClass outClass = new OutClass();
        System.out.println(outClass.c);//就近原则

        new Out(){
            @Override
            public void test() {
                System.out.println("创建一个匿名类，再次重写");
            }
        }.test();  //注意格式



        IA a = new IA(){
            @Override
            public void test() {
                System.out.println("这个类实现了 IA 接口 同时重写了test这个方法");
            }
        };
        a.test();
    }



    public void testMethod(){
        //局部内部类   出了花括号就不可使用
        class Inner {
            public int data1;

            public void func() {
                System.out.println("func");
            }
        }
        Inner inner = new Inner();
        inner.func();
    }

}
