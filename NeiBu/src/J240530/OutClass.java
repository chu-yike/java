package J240530;



//可将一个类定义在另一个类或者一个方法的内部，前者称为内部类，后者称为外部类
//内部类：
// 1.静态内部类
// 2.实例内部类
// 3.匿名内部类
// 4.局部内部类（简单讲，几乎不用

//注意：
// 1.定义在class 类名{}花括号外部的，即使是在一个文件里，都不能称为内部类
// 2.内部类和外部类共用一个Java源文件，但是经过编译之后，内部类会形成单独的字节码文件



//外部类
public class OutClass {
    public int c=11;
    static int y=21;
    public int k=7;

    public void test(){
        System.out.println("外部类方法被执行");
    }

    //静态内部类
    static class InnerClass{
        public int c=4;
        //重写
        public void test(){
            System.out.println("内部类方法被执行");
            System.out.println(c);//就近原则
        }
        //静态内部类当中，不能直接访问外部类的非静态的数据成员（还得通过实例化一个对象来访问）
    }


    static class INInnerClass {
        public int c=1;

        public void test(){
            System.out.println(INInnerClass.this.c);
            //可理解为：对于同名变量或方法，实例内部类包含两个this，一个是内部类自己的，一个是外部类的。后者可通过外部类.this.什么来访问
        }
    }


}
