class Student {
    public String name;
    public int age;
    public String StuNum;//成员变量，有一个默认值

    public void Xue() {
        System.out.println(this.name+"学就完了");
    }
    //this的用法：
    //this.成员变量
    //this.成员方法
    //this()调用当前其他对象的构造方法


    // 构造方法：
    // 1.名字必须与类名相同
    // 2.无返回值
    // 3.实例化对象的时候一定会调用构造方法，进行初始化。在对象的生命周期内只能调用一次（相当于人的出生
    // 4.构造方法不止一个，可以进行重载

    //如果当前类中没有提供任何构造方法，Java会默认提供一个不带参数的构造方法
    //一旦有了其他构造方法，Java就不会再提供不带参数的构造方法（所以下列student2，不加this的话，就不会调用不带参数的构造方法
    public Student(){
        this("cyk",21,"232");//this必须是构造方法中的第一条语句，通过this调用其他构造方法
        System.out.println("不带参数的构造方法");
    }

    public Student(String name, int age, String StuNum){
        //this引用指向当前对象(成员方法运行时调用该成员方法的对象)，在成员方法中所有成员变量的操作，都是通过该引用去访问
        this.name=name;
        this.age=age;
        this.StuNum=StuNum;
        System.out.println("调用带有三个参数的构造方法");
    }
}


public class This {

    public static void main(String[] args) {
        Student student1 = new Student();//对象的实例化，引用date1指向Date这个对象-------调用不带参数的构造方法
        //生成对象至少两步：1.为对象分配内存  2.调用合适的构造方法

        /*student1.name="褚伊珂";
        student1.age=20;
        student1.StuNum="232016027";*/
        //student1.Xue();

        Student student2=new Student("chuyike",20,"232016027");//-----------调用带三个函数的构造方法
        
    }
}
