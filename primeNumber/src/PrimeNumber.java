import java.util.Scanner;

public class PrimeNumber {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int i=2;
        for(;i<=Math.sqrt(n);i++){     //Java中，sqrt开方
            if (n%i==0){
                break;
            }
        }
        if(i>Math.sqrt(n)){
            System.out.println(n+"是素数");
        }else{
            System.out.println(n+"不是素数");
        }
    }
}
