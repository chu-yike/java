public class jvariable {
    public static void main(String[] args) {
        //无论是多少位系统，Java中数据类型占用的字节数是一定的。优点：可移植性高
        //Java中必须给变量初始化，否则会报错
        //避免混淆，以下均为标准方法，不给出错例

        //整型变量 byte(新)，short，int，long
        //整型变量int
        int a=7;
        //int型变量所能表示的范围-2^31~2^31-1
        System.out.println(Integer.MIN_VALUE);
        System.out.println(Integer.MAX_VALUE);
        //注意事项：
        //int型占4个字节，必须初始化，初值不得超过int型表示范围，否则会溢出，int的包装类型为Integer

        //长整型变量long
        long b=11L;//为区分int和long，一般建议：long类型变量的初始值后加L(或l))
        //long型变量所能表示的范围-2^63~2^63-1
        System.out.println(Long.MIN_VALUE);
        System.out.println(Long.MAX_VALUE);
        //注意事项：
        //long型初始值后推荐加L，占8个字节，包装类为Long

        //短整型变量short
        short c=21;
        //short型变量所能表示的范围-2^15~2^15-1
        System.out.println(Short.MIN_VALUE);
        System.out.println(Short.MAX_VALUE);
        //注意事项：
        //short型占2个字节，包装类型为Short

        //字节型变量byte
        byte d=21;
        //byte型变量所能表示的范围-2^7~2^7-1
        System.out.println(Byte.MIN_VALUE);
        System.out.println(Byte.MAX_VALUE);
        //注意事项：
        //byte型占1个字节，包装类型为Byte


        //浮点型变量
        //双精度浮点型double
        double e=1.1;
        //在Java中，int型/int型仍然是int(直接舍弃小数部分)
        System.out.println(e*e);
        //输出：1.2100000000000002小数不能精确表示每一位，只能说精确表示后几位
        //注意事项：
        //double型占8个字节，浮点数与整数在内存中的存储方式不同，不能单纯使用2^n的形式来计算，布局遵守IEEE 754(同C)，包装类型为Double

        //单精度浮点型float
        float f=11.21F;//在初始化值后加F来标识类型
        //注意事项：
        //float型占4个字节，同样遵循IEEE 754标准，不太推荐用float，一般工程上用到浮点数优先考虑double，包装类型为Float


        //字符型变量char
        char ch1='C';
        char ch2='6';
        System.out.println(ch1);
        System.out.println(ch2);
        //注意：Java中的字符可以存放整型
        char ch3='褚';
        System.out.println(ch3);
        //注意事项：
        //1.Java中使用  单引号+单个字母 的形式表示字符字面值
        //2.计算机的字符本质上是一个整数，在C中使用ASCII表示字符，而Java中使用Unicode表示字符。一个字符占用两个字节，后者表示的字符种类更多，包括中文
        //3.char的包装类型为Character
        //执行javac时可能会出现以下错误
        //-----------------------------
        //Test.java:3:错误:未结束的字符文字
        //char ch='霅'
        //         ^
        //-----------------------------
        // 加上  -encoding UTF-8 即可


        //布尔型变量boolean
        boolean g=true;
        boolean h=false;
        //注意事项：
        //1.boolean类型的变量只有两种取值，true表示真，false表示假。Java中boolean型和int型不能相互转化，不存在1表示true，0表示false
        //2.java虚拟机规范中，并没有明确规定boolean占几个字节，也没有专门用来处理boolean的字节码指令，在Oracle公司的虚拟机实现中，boolean占1个字节
        //3.boolean的包装类型为Boolean
    }
}
