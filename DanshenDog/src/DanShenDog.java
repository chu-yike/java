public class DanShenDog {

    //寻找单身狗，给定一个非空整数数组，除了某个元素只出现一次以外，其余每个元素均出现两次。找出那个只出现了一次的元素
    public static int dog(int[] array){
        int ret=0;
        for (int i = 0; i < array.length; i++) {
            ret^=array[i];    //异或，二进制中每位数字相同为0，不同为1，再以二进制形式输出
        }return ret;
    }

    public static void main(String[] args) {
        int[] array={1,2,2,1,3};
        System.out.println(dog(array));
    }
}
