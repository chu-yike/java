import java.util.Scanner;

public class leapyear {
    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        int year = sc.nextInt();
        if(year%100==0){
            //判断世纪闰年
            if(year%400==0){
                System.out.println("是闰年");
            }else{
                System.out.println("不是闰年");
            }
        }else{
            //判断普通闰年
            if(year%4==0){
                System.out.println("是闰年");
            }else{
                System.out.println("不是闰年");
            }
        }
    }// ?  sc.close();
}
