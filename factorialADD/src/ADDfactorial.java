import java.util.Scanner;

public class ADDfactorial {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int num=sc.nextInt();
        int sum=0;
        while(num>0){
            int fac=1;
            int tmp=num;
            //外层循环负责求阶乘的和
            while(tmp>0) {
                fac *= tmp;
                tmp--;
                //内层循环求阶乘
            }
            sum+=fac;
            num--;
        }
        System.out.println("sum="+sum);
    }
}
