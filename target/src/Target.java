import java.util.Arrays;

public class Target {
    //给定一个整数数组nums和一个整数目标值 target，请你在该数组中找出和为目标值target的那两个整数，并返回它们的数组下标。
    //你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。

    public static int[] func2(int[] array,int target){
        int[] ret ={-1,-1};
        for (int i = 0; i < array.length; i++) {     //暴力算法，遍历找到和为target的两个数，得到数组下标，储存到另一个数组中，返回
            for (int j = 0; j < i+1; j++) {
                if(array[i]+array[j]==target){
                    ret[0]=i;
                    ret[1]=j;
                }
            }
        }return ret;
    }

    public static void main(String[] args) {
        int[] array={1,2,3,4,5,6,7,8,9};
        int[] ret = func2(array,10);
        System.out.println(Arrays.toString(ret));
    }
}