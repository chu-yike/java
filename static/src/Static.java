class Date{
    public int year;
    public int month;
    public int day;
    static int R = 21;


    //静态成员变量的初始化
    // 1.就地初始化：定义时直接给出初始值
    //   private static year = 2024;
    // 2.通过getter and setter方法初始化
    // 3.构造方法也可（一般使用较少
    // 4.代码块
    //   4.1静态代码块（定义在类当中，方法的外面，一般用来初始化静态成员变量）  双亲委派模型：静态代码块只执行一次，不重复执行。这个类只执行一次
    //      <1>当类加载的时候被执行，若出现多个静态代码块，和定义顺序有关
    //      <2>执行顺序：  静态代码块->实例代码块->构造代码块    执行顺序和三者定义顺序无关，但和这三类各自其中的定义顺序有关
    //   4.2非静态代码块/实例代码块/构造代码块     主要4.1 4.2
    //   4.3同步代码块
    static {
        R=11;  //4.2静态代码块
    }


    //static修饰的成员变量称为静态变量，且不可被更改。不依赖于对象，可直接通过类名来访问
    //特性：
    // 1.所有对象共享
    // 2.推荐用类名访问
    // 3.储存在方法区当中
    // 4.随类的加载而创建，卸载而销毁


    public Date(int y,int m,int d){
        this.year=y;
        this.month=m;
        this.day=d;
    }

    public void show(){
        System.out.println(this.year+"年"+this.month+"月"+this.day+"日"+this.R);
    }


    //static修饰的成员方法成为静态成员方法，是类的方法，不是某个对象所特有的
    //特性：（和修饰成员变量特性差不多，以下来补充）
    // 1.不能在静态方法中访问任何非静态成员变量
    // 2.静态方法中不能调用任何非静态方法。 （但非静态方法中可以调用静态方法）其实很好理解，static不依赖于对象，而this依赖对象
    // 因为静态方法不依赖于对象，可直接通过类名访问。非静态方法依赖对象，需要通过对象的引用访问。
    // 即：静态方法中不能出现this。
    //虽然不能直接调用，但可以实例化

    public static int getDate(){
        // this.day=0;error  static修饰的成员方法中不可出现this
        /*
        show();
        return R;    //error:无法从静态上下文中引用非静态方法show()
        */

        //可通过以下方法
        Date date1=new Date(2024,5,19);
        date1.show();
        return R;//可通过实例化new一个新的对象，通过对象的访问得到R
    }


}

public class Static {
    public static void main(String[] args) {
        Date date = new Date(2024,5,18);
        System.out.println(Date.R);//可直接通过类名访问
    }
}
