public class jNarcissisticNumber {
    public static void main(String[] args) {
        for (int i = 100; i < 999; i++) {
            int count=0;
            int tmp=i;
            while(tmp!=0){
                count++;       //通过改变i的值，可求出多位水仙花数。这里的count是位数，也是每位数的次方数
                tmp/=10;
            }
            tmp=i;
            int sum=0;
            while(tmp!=0){
                sum+=Math.pow(tmp%10,count);
                tmp/=10;
            }
            if(sum==i){
                System.out.println(i);
            }
        }
    }
}//水仙花数：一个三位数，各位数字的立方和(位数)等于该数字本身
