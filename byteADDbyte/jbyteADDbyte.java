public class jbyteADDbyte {
    public static void main(String[] args) {
        byte a=1;
        byte b=2;
        //byte c=a+b;
        int c=a+b;
        //或者使用强制类型转化byte c=(byte)(a+b);
        System.out.println(c);

    }
}
//由于计算机的CPU通常是按照4个字节为单位从内存中读写数据，为了硬件上实现方便
//诸如byte，short这种低于4个字节的类型，通常会先提升为4个字节(int)，再参与运算

//如上例，虽然a，b都是byte型，但计算a+b时会先将a和b都提升为int型，再进行运算，得到的结果也是int型，需把c定义为int型
// 使用byte,short时要多加思考

