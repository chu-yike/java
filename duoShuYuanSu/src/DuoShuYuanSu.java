import java.util.Arrays;

public class DuoShuYuanSu {

    public static int DuoShu(int[] array){
        Arrays.sort(array);      //排序，多数元素必是中位数
        return array.length/2;   //返回中位数
    }

    public static void main(String[] args) {
        int[] array={11,21,12,21,7};
        System.out.println(DuoShu(array));
    }
}
