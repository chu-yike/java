package J0529;

import java.util.Objects;


public class Dog extends Animal implements ISwimming,IRunning{   //狗属于动物会游泳、会跑

    public Dog(String name,int age) {
        super(name,age);                                     //对父类进行初始化
    }

    public void eat() {
        System.out.println(this.name+" 正在吃狗粮...");        //重写父类构造方法
    }

    public void bark() {
        System.out.println(this.name + " 正在汪汪汪叫.....");   //定义新的构造方法
    }


    public void run() {
        System.out.println(this.name +" 正在跑.....");
    }

    public void swim() {
        System.out.println(this.name +" 正在游泳.....");
    }

    public  int hashCode() {
        return Objects.hash(name,age);                  //
    }


    public boolean equals(Object obj) {
        if(obj == null) {
            return false;
        }
        if(this == obj) {
            return true;
        }
        //是不是同一个类型
        if(!(obj instanceof Dog)) {          //instanceof是Java中一个二元操作符，也是保留关键字。
                                             // 用于测试左边的对象是否是右边指定类的实例或其子类的实例。
            return false;
        }

        Dog tmp = (Dog)obj;
        return tmp.name.equals(this.name)
                && tmp.age == this.age;
    }


}
