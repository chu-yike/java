package J0529;

//定义Animal为抽象类
public abstract class Animal {
    public String name;
    public int age;

    public Animal(String name,int age){
        this.age=age;
        this.name=name;
    }

    public abstract void eat();//抽象类方法

}
