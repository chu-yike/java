public class LianXuJiShu {

    public static boolean LianXu(int[] array){
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if(array[i] %2 != 0){
                count++;       //计数器，奇数就加一
                if(count==3){
                    return true;
                }
            }else{
                count=0;  //不是三个奇数就重置为0
            }
        }
        return false;
    }

    public static void main(String[] args) {
        int[] array={11,21,7,12,14};
        System.out.println(LianXu(array));
    }
}
