import java.util.Arrays;

public class JiShuzaiqian {
    public static void func1(int[] array){
        int left = 0;
        int right = array.length-1;
        while(left<right&&array[left]%2!=0){     //如果从左边开始不是偶数，元素无需后移，访问下一个元素
            left++;
        }
        while(left<right&&array[right]%2==0){    //如果右边是偶数，元素无需左移，访问下一个
            right--;
        }
        swap(array,left,right);
    }

    public static void swap(int[] array,int i,int j){    //反之，调换顺序
        int tmp=array[i];
        array[i]=array[j];
        array[j]=tmp;
    }

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        func1(array);
        System.out.println(Arrays.toString(array));
    }
}
