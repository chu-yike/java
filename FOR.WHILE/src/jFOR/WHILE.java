package jFOR;

public class WHILE {
    public static void main(String[] args) {
        //1~100中数字9的个数
        int count=0;
        for(int i=0;i<=100;i++){
            if(i%10==9){//个位9
                count++;
            }
            if(i/10==9){//十位9
                count++;
            }
        }
        System.out.println(count);
    }
}
