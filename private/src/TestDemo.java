class Student{
    //被private修饰的只能在当前类中访问，不可修改
    private String name="CYK";
    private int age=1;
    private static int classname=113;//static修饰类中的静态成员变量，是共用的

    public Student(String name,int age){
        this.name=name;
        this.age=age;
    }

    public void print(){
        System.out.println("姓名"+this.name+"年龄"+this.age+"班级"+this.classname);
    }


    //以下，鼠标右键Generate，再点击Getter and Setter，即可用编译器自动生成构造方法，给每个name，age进行赋值
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}


public class TestDemo {
    public static void main(String[] args) {
        //Student student=new Student();//这里会报错，private修饰的变量只能在类内使用，此处相当于没有值，访问不到初始化的值
        //System.out.println(student.getName());


        Student student = new Student("褚",2);//实例化对象
        //正常来说，被private修饰的成员变量被封装起来了，只能在当前类中(此时Student中)被使用，类外不可使用
        //但可以通过创建public类，如上的getter，setter，来给成员变量赋值,访问到
        student.setName("褚伊珂");
        student.setAge(20);
        System.out.println(student.getName());
        System.out.println(student.getAge());//输出赋的值
    }
}
