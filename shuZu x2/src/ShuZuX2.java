import java.util.Arrays;

public class ShuZuX2 {

    //实现一个方法 transform, 以数组为参数, 循环将数组中的每个元素乘以2,并设置到对应的数组元素上.
    // 例如原数组为 {1, 2, 3}, 修改之后为 {2, 4, 6}
    public static int[] transform(int[] array){
        for (int i = 0; i < array.length; i++) {
            array[i]*=2;
        }
        return array;
    }

    public static void main(String[] args) {
        int[] array={11,21,12,14};
        System.out.println(Arrays.toString(transform(array)));
    }
}
