//继承机制：是面向对象程序设计使代码可以重复利用的最重要的手段，它允许程序员在保持原有类特性的基础上进行扩展，增加新功能，这样产生的类叫做派生类。
// 继承主要解决的问题是：公共性的抽取，实现代码的复用

//父类：基类，超类
// 子类：派生类       二者要在一个包里


import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

public class Animal{
    String name;
    int age;
    int a=21;

    public Animal(String name,int age){
        this.name=name;
        this.age=age;
        System.out.println("Animal：构造方法执行");
    }

    {
        System.out.println("Animal：实例化代码块执行");
    }

    static{
        System.out.println("Animal：实例化代码块执行");
    }

    class JiCheng{
        public JiCheng(){
            //文件名
            // 当没有提供任何构造方法时，JAVA中会默认提供上方代码。但假如写了任何一个构造方法，就不会默认提供
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void eat(){
        System.out.println(name+"吃饭");
    }
    public void sleep(){
        System.out.println(name+"睡觉");
    }


}
