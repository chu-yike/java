public class Test {
    public static void main(String[] args) {
        Dog dog=new Dog();//Dog类中没有定义任何成员变量，name和age属性都是从Animal中继承来的
        dog.setName("宝宝");//给子类继承到的父类的成员变量赋值
        //也可以如下写
        dog.name="BB";
        System.out.println(dog.getName());
        dog.eat();//调用继承的方法
        dog.bark();
    }
}
