//

//Java中若要表示类之间的继承关系，需要借助extends关键字
//修饰符 class 子类 extends 父类{
//   //……
// }

public class Dog extends Animal{
    int a=11;
    //子类中访问父类的成员变量或成员方法，均遵循——就近原则。
    // 就近原则：子类与父类中成员变量或成员方法同名时，在子类方法或通过子类对象访问方法时，优先访问自己的
    //         自己没有再到父类中找，父类中没有则报错

    //子类对象中成员是有两部分组成的天基类继承下来的以及子类新增加的部分。父子父子肯定是先有父再有子
    //所以在构造子类对象时候，先要调用基类的构造方法，将从基类继承下来的成员构造完整
    //然后再调用子类自己的构造方法，将子类自己新增加的成员初始化完整
    // 总之：使用子类前，先帮助父类初始化

    void bark(){
        System.out.println(name+"汪汪汪");
    }

    public Dog(String name,int age){
        super(name,age);
        //当子类继承父类之后，子类需要显示地调用父类的构造方法，需要先帮助父类成员进行初始化

        System.out.println("Dog：构造方法执行");
    }

    {
        System.out.println("Dog：实例化代码块执行");
    }

    static {
        System.out.println("Dog：实例化代码块执行");
    }
    //执行顺序：
// 先执行父类和子类的静态
// 再执行父类的实例和构造
// 最后执行子类的实例和构造
// ！但无论实例化多少个对象，静态的只执行一次




    public Dog(){
        super("haha",10);
        //调用父类的构造方法
    }
}

//注：
// 1.子类会将父类中的成员方法和成员变量继承到子类中
// 2.子类继承父类后，必须要新添加自己特有的成员，体现出与基类的不同，否则继承无意义



