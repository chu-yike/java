package com.cyk.www;

public class TestDemo {

    //此处的a没有public等修饰，为默认权限。当前a只能在同一个包中访问
    int a=7;

    //此处成员方法是public修饰，包外，类外，都可以访问
    public void Test(){
        //同一个包，同一个类，访问默认权限a
        System.out.println("test()…"+a);
    }
}
