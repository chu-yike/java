import java.util.Scanner;

public class jScanner {
    public static void main1(String[] args) {
        Scanner sc=new Scanner(System.in);

        System.out.println("请输入你的姓名：");
        String name = sc.nextLine();         //读入一个字符串
        //----               ----   //大同小异，依据数据类型

        System.out.println("请输入你的年龄：");
        int age = sc.nextInt();              //读入一个整型

        System.out.println("请输入你的工资：");
        float salary = sc.nextFloat();       //读入一个float型

        System.out.println("你的信息如下：");
        System.out.println("姓名："+name+"\n"+"年龄："+age+"\n"+"工资："+salary);

    }

    public static void main(String[] args) {
        //循环输入
        
    }
}
